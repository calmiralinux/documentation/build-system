# Build System Handbook

- **Version:** 1.0
- **License:** [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/)
  ([plain text here](LICENSE))
- **Authors:** Michail Krasnov <linuxoid85@gmail.com>

## Languages

- [X] [Russian](ru/book/index.html)
- [ ] English
- [ ] Belorussian
- [ ] Ukrainian

## Install & read

- Install `cargo` [Rust].
- Install mdbook, mdbook-admonish and mdbook-pdf:

```bash
for pkg in 'mdbook' 'mdbook-pdf' 'mdbook-admonish'; do
  cargo install $pkg
done
```

- Go to the directory with handbook:

```
# Russian handbook
cd ru
# English handbook
cd en
# Ukrainian handbook
cd ua
# Belorussian handbook
cd by
```

- Run:

```
mdbook build
mdbook serve --open
```

## Инструкция по переводу

1. Если вы не входите в команду разработчиков Calmira GNU/Linux-libre, то
   сделайте форк этого репозитория и склонируйте его себе на компьютер.
2. Если вы входите в команду разработчиков Calmira GNU/Linux-libre, склонируйте
   этот репозиторий себе на компьютер.
3. Сделайте отдельную ветку для работы: `git branch translate/LANGUAGE && git
   checkout translate/LANGUAGE` (замените `LANGUAGE` на тот язык, на который
   переводите (например, `en` для английского, `ua` для украинского, `by` для
   беларусского и т.д.).
4. Скопируйте директорию с оригинальным руководством (на русском языке): `cp -r
   ru LANGUAGE` (замените `LANGUAGE` на имя того языка, на который переводите).
5. В директории `LANGUAGE/src` содержатся Markdown-файлы с документацией. Это и
   следует переводить.
    - В файле `LANGUAGE/src/SUMMARY.md` содержится содержание руководства и
      список всех страниц со ссылками на них. Это также переводится.
6. В заключение делаете коммит и отправляете изменения в наш репозиторий. После
   чего заходите на GitLab и делаете Merge Request:

```bash
git add .
git commit -m "Добавление перевода на <LANGUAGE> язык" # Замените <LANGUAGE> на язык, на который вы переводили руководство
git push --set-upstream origin translate/LANGUAGE
```

### FAQ

**Почему копируется директория с русским переводом руководства?**

> Потому что русский язык - основной язык автора этого руководства, поэтому
> HandBook написан именно на русском языке. Самая актуальная версия руковосдства
> всегда русская.

**Почему используется mdBook в кач-ве генератора документации?**

> mdBook - это достаточно простой и удобный генератор документации.
