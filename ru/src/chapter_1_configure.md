# Конфигурирование сборочных инструкций

Каждый режим сборки описывается в отдельном конфигурационном файле. Конечно,
можно реализовать всё это в одном конфиге, но тогда он будет достаточно большим:
вносить правки в такой конфиг будет затруднительно. Поэтому было принято решение
разделить этот файл на несколько небольших.

## Пример конфигурационного файла

```toml
[settings]
dir = "core"
path = "/bin:/sbin:/usr/bin:/usr/sbin"
calm = "/mnt/calm"
calm_tgt = "x86_64-calm-linux-gnu"
makeflags = "-j4"
src_dir = "/mnt/calm/usr/src"

[build]
packages = [
  "acl",
  "attr",
  "shadow",
  "gcc"
]
```

Конфигурационный файл имеет 2 секции: `settings` и `build`.

### `settings`

Эта секция используется сборочными инструкциями, на её основе им передаются
специфичные переменные окружения.

| Переменная  | Тип данных | Описание                                                                                                                                     |
| ----------- | ---------- | -------------------------------------------------------------------------------------------------------------------------------------------- |
| `dir`       | `String`   | Имя поддиректории в `instructions/` со сборочными инструкциями для данного режима                                                            |
| `path`      | `String`   | Переменная окружения `PATH` для сборочных скриптов                                                                                           |
| `calm`      | `String`   | Описывает префикс для установки программного обеспечения. Используется, как правило, только при сборке временного инструментария (toolchain) |
| `calm_tgt`  | `String`   | Используется для сборки кросс-компилятора                                                                                                    |
| `makeflags` | `String`   | Ключи для системы сборки GNU Make                                                                                                            |
| `src_dir`   | `String`   | Путь до директории с исходным кодом необходимого программного обеспечения                                                                    |

### `build`

Секция содержит список необходимого для сборки в указанном порядке программного
обеспечения.

| Переменная | Тип данных    | Описание                                      |
| ---------- | ------------- | --------------------------------------------- |
| `packages` | `Vec<String>` | Список пакетов для сборки в указанном порядке |
