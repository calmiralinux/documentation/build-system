# Установка системы сборки

## Сборка

Используйте систему сборки `cargo`:

```bash
cargo build --release
```

## Установка

Скопируйте исполняемый файл в `/usr/bin`:

```bash
sudo cp -v target/release/bs /usr/bin/bs
```

Теперь необходимо скопировать скрипт `bs.sh` с общими функциями в `/etc`:

```bash
sudo cp -v src/bs.sh /etc
```
