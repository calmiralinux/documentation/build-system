# Содержание

- [Часть 1](./chapter_1.md)
  - [Установка системы сборки](./chapter_1_installation.md)
  - [Использование системы сборки](./chapter_1_usage.md)
  - [Конфигурация системы сборки](./chapter_1_configure.md)
  - [Создание сборочных инструкций](./chapter_1_build_scripts.md)
- [Часть 2](./chapter_2.md)
